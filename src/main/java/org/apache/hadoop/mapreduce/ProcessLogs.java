package org.apache.hadoop.mapreduce;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.SequenceFile.CompressionType;
import org.apache.hadoop.io.compress.BZip2Codec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class ProcessLogs extends Configured implements Tool {

	public static void main(String[] args) throws Exception {

		Configuration conf = new Configuration();
		conf.set("output_format",args[4]);
		conf.set("ip_address", args[5]);

		/*
		 * set the caseSensitive configuration value for the job programmatically. Comment code out to set from the command line
		 * instead.
		 */
		 conf.setBoolean("caseSensitive", false);

		int exitCode = ToolRunner.run(conf, new ProcessLogs(), args);
		System.exit(exitCode);
	}

	public int run(String[] args) throws Exception {
		/*
		 * Validate that two arguments were passed from the command line.
		 */

		if (args.length != 4) {
			System.out.printf("Usage: ProcessLogs <input dir> <output dir> <output_format> <ip_address>\n");
			System.exit(-1);
		}

		/*
		 * Instantiate a Job object for your job's configuration.
		 */
		Job job = Job.getInstance(getConf());

		/*
		 * Specify the jar file that contains your driver, mapper, and reducer. Hadoop will transfer this jar file to nodes in
		 * your cluster running mapper and reducer tasks.
		 */
		job.setJarByClass(ProcessLogs.class);

		/*
		 * Specify an easily-decipherable name for the job. This job name will appear in reports and logs.
		 */
		job.setJobName("Process Logs");

		/*
		 * Specify the paths to the input and output data based on the command-line arguments.
		 */
		FileInputFormat.setInputPaths(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		/*
		 * Specify the mapper and reducer classes.
		 */
		job.setMapperClass(LogDayMapper.class);
		job.setReducerClass(CountReducer.class);
		//job.setCombinerClass(CountReducer.class);
	

		if(args[2].equals("text")){
			job.setMapOutputKeyClass(Text.class);
			job.setMapOutputValueClass(Text.class);
		
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(IntWritable.class);
		}
		if(args[2].equals("sequence_file")){
		    job.setOutputFormatClass(SequenceFileOutputFormat.class);
		    //set compression options
		    FileOutputFormat.setCompressOutput(job, true);
		    //use BZipCodec2
		    FileOutputFormat.setOutputCompressorClass(job, BZip2Codec.class);
		    //use block compression
		    SequenceFileOutputFormat.setOutputCompressionType(job,CompressionType.BLOCK);
		}
		//set number of reducers to 7
		job.setNumReduceTasks(7);

		/*
		 * Specify the partitioner class.
		 */
		job.setPartitionerClass(DayPartitioner.class);
		
//		if (job.getCombinerClass() == null) {
//			throw new Exception("Combiner not set");
//		}
		boolean success = job.waitForCompletion(true);
		return (success ? 0 : 1);
	}
}