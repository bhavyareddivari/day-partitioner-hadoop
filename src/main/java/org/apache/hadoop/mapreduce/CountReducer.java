package org.apache.hadoop.mapreduce;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/* Counts the number of values associated with a key */

public class CountReducer extends Reducer<Text, Text, Text, Text> {

	@Override
	public void reduce(Text key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException {
			
		int count = 0;		
		for (@SuppressWarnings("unused")
		Text value : values) {
			count++;
		}
		String s=Integer.toString(count);
		//System.out.println(s);
		context.write(key, new Text(s));
	}
}
