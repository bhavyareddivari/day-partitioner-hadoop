package org.apache.hadoop.mapreduce;

import java.util.HashMap;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.conf.Configurable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Partitioner;

public class DayPartitioner<K2, V2> extends Partitioner<Text, Text> implements
    Configurable {

  private Configuration configuration;
  HashMap<String, Integer> day = new HashMap<String, Integer>();

  /**
   * Set up the months hash map in the setConf method.
   */
  public void setConf(Configuration configuration) {
    this.configuration = configuration;
    day.put("Sun", 0);
    day.put("Mon", 1);
    day.put("Tue", 2);
    day.put("Wed", 3);
    day.put("Thu", 4);
    day.put("Fri", 5);
    day.put("Sat", 6);
  }

  /**
   * Implement the getConf method for the Configurable interface.
   */
  public Configuration getConf() {
    return configuration;
  }

  /**
   * You must implement the getPartition method for a partitioner class.
   * This method receives the three-letter abbreviation for the day
   * as its value. (It is the output value from the mapper.)
   * It should return an integer representation of the month.
   * 
   * For this partitioner to work, the job configuration must have been
   * set so that there are exactly 7 reducers.
   */
  public int getPartition(Text key, Text value, int numReduceTasks) {
    return (int) (day.get(value.toString())+1);
  }
}
