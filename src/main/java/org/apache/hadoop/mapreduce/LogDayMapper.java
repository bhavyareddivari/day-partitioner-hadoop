package org.apache.hadoop.mapreduce;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class LogDayMapper extends Mapper<LongWritable, Text, Text, Text> {
	
	public static List<String> dates = Arrays.asList("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
	 String days;
	 String output;
	 String inputip;
	/**
   * Example input line:
   * 96.7.4.14 - - [24/Apr/2011:04:20:11 -0400] "GET /cat.jpg HTTP/1.1" 200 12433
   *
   */
  @Override
  public void map(LongWritable key, Text value, Context context)
      throws IOException, InterruptedException {
    String ip=null;
    /*
     * Split the input line into space-delimited fields.
     */
	  output=context.getConfiguration().get("output_format");
      inputip=context.getConfiguration().get("ip_address");
      //System.out.println(output + " "+ipaddress);
    String[] fields = value.toString().split(" ");
    ip = fields[0];
    if (fields.length > 3) {
      if(inputip.equals("all")){
      /*
       * Save the first field in the line as the IP address.
       */
      writeIp(ip,fields[3],context);
    }
      else{
    	  if(inputip.equals(ip)){
    	      writeIp(ip,fields[3],context);
    		  
    	  }
      }
  }
  } 
  private void writeIp(String ip,String str,Context context) throws IOException, InterruptedException
  {
	  
	  String[] dtFields = str.split(":");
      if (dtFields.length > 1) {
        dtFields[0]=dtFields[0].substring(1, dtFields[0].length()-1);        
        try{
        	SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MMM/yyyy");
        	java.util.Date myDate= dateFormat.parse(dtFields[0]);
        	SimpleDateFormat format2=new SimpleDateFormat("EEE");
        	days=format2.format(myDate);
        	//System.out.println(days);
        }
        catch(Exception e){
        	e.printStackTrace();
        }
        
        /* check if it's a valid day, if so, write it out */
        if (dates.contains(days))
        	context.write(new Text(ip), new Text(days));
      }
	  
  }
  
}
